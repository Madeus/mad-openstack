#!/usr/bin/env python3

"""
This script describes different assemblies to deploy. The desired assembly is
selected by given the '-t|--test' argument when calling this script. OpenStack
component class are dynamically imported depending on the desired test.
"""

import os, sys
import argparse
import importlib
from assembly import Assembly


# We define the colors used to plot the Gantt diagram:
# http://www.perbang.dk/rgbgradient/
colors = dict(Nova = '#E6194B',
              Neutron = '#3CB44B',
              Glance = '#FFE119',
              OpenVSwitch = '#0082C8',
              Keystone = '#F58231',
              MariaDB = '#911EB4',
              RabbitMQ = '#46F0F0',
              MemCached = '#F032E6',
              HAProxy = '#D2F53C',
              Common = '#FABEBE',
              Facts = '#000000')


parser = argparse.ArgumentParser()
parser.add_argument('-n', '--dry-run',
        choices=[0, 1, 2, 3], default = 0, dest='dry_run', type=int,
        help='Can be used to fire transitions in different ways: (0) normal \
                run (default); (1) with threads and fake ssh; (2) with \
                threads but no ssh; (3) without threads nor ssh.')
parser.add_argument('-t', '--test', dest='test', required=True,
        choices=['seq_1t', 'fj_1t', 'dag_1t', 'dag_2t', 'dag_nt', 'dag_nt2',
            'dag_nt3', 'dag_nt4', 'dag_nt5', 'dag_nt6', 'dag_nt7', 'dag_nt8'],
        help='Select the test/assembly to run/deploy.')
parser.add_argument('-v', '--verbose',
        action='store_true', default = False, dest='verbosity',
        help='Set verbosity')

args = parser.parse_args()
args.dry_run = int(args.dry_run)

# Import the appropriate component definitions:
module_names = ['facts', 'common', 'haproxy', 'memcached',
        'rabbitmq', 'mariadb', 'keystone', 'glance', 'nova', 'openvswitch',
        'neutron']

class_names = ['Facts', 'Common', 'HAProxy', 'MemCached',
        'RabbitMQ', 'MariaDB', 'Keystone', 'Glance', 'Nova', 'OpenVSwitch',
        'Neutron']

for module_name, class_name in zip(module_names, class_names):
    test_modules = 'components.openstack_' + args.test
    test_module = test_modules + '.' + module_name
    module = importlib.import_module(test_module)
    my_class = getattr(module, class_name)
    exec("%s = %s" % (class_name, 'my_class'))

title = ''

def connect_seq_1t(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('common', 'haproxy')
    assembly.auto_connect('haproxy', 'memcached')
    assembly.auto_connect('memcached', 'mariadb')
    assembly.auto_connect('mariadb', 'rabbitmq')
    assembly.auto_connect('rabbitmq', 'keystone')
    assembly.auto_connect('keystone', 'glance')
    assembly.auto_connect('glance', 'nova')
    assembly.auto_connect('nova', 'openvswitch')
    assembly.auto_connect('openvswitch', 'neutron')

    title = 'Sequential 1-Transitional OpenStack Deployment'

def connect_fj_1t(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')
    assembly.auto_connect('rabbitmq', 'mariadb')
    assembly.auto_connect('memcached', 'mariadb')
    assembly.auto_connect('openvswitch', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'Fork-join 1-Transitional OpenStack Deployment'

def connect_dag_1t(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG 1-Transitional OpenStack Deployment'

def connect_dag_2t(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG 2-Transitional OpenStack Deployment'

def connect_dag_nt(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt2(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt3(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt4(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt5(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt6(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt7(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

def connect_dag_nt8(assembly):
    assembly.auto_connect('facts', 'common')
    assembly.auto_connect('facts', 'haproxy')
    assembly.auto_connect('facts', 'rabbitmq')
    assembly.auto_connect('facts', 'memcached')
    assembly.auto_connect('facts', 'openvswitch')

    assembly.auto_connect('facts', 'nova')
    assembly.auto_connect('facts', 'neutron')
    assembly.auto_connect('facts', 'glance')

    assembly.auto_connect('common', 'mariadb')
    assembly.auto_connect('haproxy', 'mariadb')

    assembly.auto_connect('mariadb', 'keystone')
    assembly.auto_connect('mariadb', 'nova')
    assembly.auto_connect('mariadb', 'glance')
    assembly.auto_connect('mariadb', 'neutron')

    # I used to set:
    # assembly.auto_connect('keystone', 'nova/glance/neutron')
    # but since they all share mariadb ports, they try to add it naively
    key.net.ports['keystone'].connect(nov.net.ports['keystone'])
    key.net.ports['keystone'].connect(gla.net.ports['keystone'])
    key.net.ports['keystone'].connect(neu.net.ports['keystone'])

    title = 'DAG n-Transitional OpenStack Deployment'

# Initialize our OpenStack components:
fac = Facts()
com = Common()
ha = HAProxy()
mem = MemCached()
rab = RabbitMQ()
mar = MariaDB()
key = Keystone()
gla = Glance()
nov = Nova()
ovs = OpenVSwitch()
neu = Neutron()

# Initialize an assembly to manage them:
assembly = Assembly([
    [fac, 'facts'],
    [com, 'common'],
    [ha, 'haproxy'],
    [mem, 'memcached'],
    [rab, 'rabbitmq'],
    [mar, 'mariadb'],
    [key, 'keystone'],
    [gla, 'glance'],
    [nov, 'nova'],
    [ovs, 'openvswitch'],
    [neu, 'neutron']])

if args.dry_run:
    assembly.set_dry_run(args.dry_run)

# Connect our components depending on the desired assembly:
method_name = 'connect_' + str(args.test)
possibles = globals().copy()
possibles.update(locals())
method = possibles.get(method_name)
if not method:
    raise NotImplementedError("Method %s not implemented" % method_name)
    sys.exit(1)
method(assembly)

# Fire the deployment process:
assembly.auto_run()
if args.dry_run == 3:
    # weird bug
    assembly.auto_run()

if os.path.isfile("result_tasks.txt"):
    assembly.automaton.per_component_results()
    assembly.automaton.plot_gantt(title, colors)

if assembly.check_dep(args.verbosity):
    sys.exit(0)
else:
    sys.exit(1)

