#!/usr/bin/env python

import os
import sys
import yaml
import plotly
from plotly.figure_factory import create_gantt


fname = sys.argv[1]
print(fname)
with open(fname, "r") as f:
    df = yaml.load(f)

if not df:
    print("No information has been fetched - leave the programm :(")
    sys.exit(1)

# http://www.perbang.dk/rgbgradient/
colors = dict(Nova = '#E6194B',
              Neutron = '#3CB44B',
              Glance = '#FFE119',
              OpenVSwitch = '#0082C8',
              Keystone = '#F58231',
              MariaDB = '#911EB4',
              RabbitMQ = '#46F0F0',
              MemCached = '#F032E6',
              HAProxy = '#D2F53C',
              Common = '#FABEBE',
              Facts = '#000000')

fig = create_gantt(df,
        colors=colors, index_col='Component',
        show_colorbar=True,
        showgrid_x=True, showgrid_y=True)
plotly.offline.plot(fig, filename=os.path.splitext(fname)[0]+'.html')

sys.exit(0)

