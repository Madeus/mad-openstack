#!/usr/bin/env python

import yaml
import os, sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
from scipy import stats
import matplotlib.pyplot as plt

def mean (list):
    if len(list) != 0:
        return sum(list)/len(list)
    else:
        return 0

if __name__ == "__main__":

    fname = sys.argv[1]
    with open(fname, "r") as f:
        results = yaml.load(f)

    arranged_results = {
       test_type:
            { tool:
                [ result["time"]
                    for result in results
                    if (result["params"]["tool"] == tool
                        and result["params"]["test_type"] == test_type
                            and result["failure"] == 0) ]
              for tool in
              [ result["params"]["tool"] for result in results ] }
        for test_type in
        [ result["params"]["test_type"] for result in results ] }

    tools = ('shell', 'mad')
    ttypes = ('seq_1t', 'dag_1t')
    #ttypes = ('seq_1t', 'fj_1t', 'dag_1t')
    # Extract test types and tool types from the result file:
    #ttypes = tuple(arranged_results.keys())
    #tools = tuple(arranged_results[ttypes[0]].keys())

    arranged_means = {}
    arranged_stds = {}

    # Compute means and standard deviations: 
    for tool in tools:
        arranged_means[tool] = {}
        arranged_stds[tool] = {}
        for ttype, tres in arranged_results.items():
            arranged_means[tool][ttype] = mean(tres[tool])
            arranged_stds[tool][ttype] = np.std(tres[tool])

    # Compute max values:
    max_values = {}
    for tool in tools:
        max_values[tool] = max(arranged_means[tool].values())

    # Populate appropriate structures for matplotlib:
    means={}
    stds={}
    for tool in tools:
        means[tool]=[]
        stds[tool]=[]
        for ttype in ttypes:
            means[tool].append(arranged_means[tool][ttype])
            stds[tool].append(arranged_stds[tool][ttype])

    fig, ax = plt.subplots()

    index = np.arange(len(ttypes))
    bar_width = 0.35

    opacity = 0.5
    error_config = {'ecolor': 'black', 'alpha': 0.8}


    rects = {}
    colors = ['b', 'r']
    i=0

    for tool in tools:
        rects[tool] = ax.bar(index + i*bar_width, means[tool], bar_width,
                            alpha=opacity, color=colors[i],
                            yerr=stds[tool], error_kw=error_config,
                            label=tool)
        i=i+1
        for rect in rects[tool]:
            height = rect.get_height()
            if height == max_values[tool]:
                txt = 1.0
            else: 
                txt = float(height/max_values[tool])
            plt.text(rect.get_x() + rect.get_width()/2.0, height*1.02,
                    '%.2f' % txt, color='gray', ha='center', va='bottom')

    ax.set_xlabel('Tests')
    ax.set_ylabel('Time (s)')
    ax.set_title('Time to deploy OpenStack for different tests')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(ttypes)
    ax.legend()

    fig.tight_layout()
    plt.savefig(os.path.splitext(fname)[0]+'.svg')

