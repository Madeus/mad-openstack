
PACKAGE_TARGETS := ansible.cfg \
	assembly.py automaton.py bench_params.yaml bench.py components \
	deploy_openstack.py enos mad.py Makefile plot_gantt.py \
	plot_results_dry-run.py plot_results_overhead.py plot_results_perf.py \
	plot_results_scalability.py rabbit_vars.yml README.md requirements.txt \
	deploy_openstack.sh utils

CTAGS := $(shell command -v ctags 2> /dev/null)
CTAGS-exists : ; @which ctags > /dev/null
PYTHON2-exists := $(shell command -v python2 2> /dev/null)
PYTHON3-exists := $(shell command -v python3 2> /dev/null)
PIP-exists := $(shell command -v pip3 2> /dev/null)
VIRTUALENV-exists : ; @which virtualenv > /dev/null

check_python3:
ifndef PYTHON3-exists
	$(error "python3 is not available - please install it.")
endif

check_pip3:
ifndef PIP-exists
	$(error "pip3 is not available - please install it.")
endif

virtualenv: check_python3 check_pip3
	@echo Check if the software 'virtualenv' is installed.
	@echo [WARNING] Using 'pip' to install it in the user directory.
	hash virtualenv 2>/dev/null || pip install --user virtualenv

venv3: virtualenv venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || virtualenv --python=python3 venv
	venv/bin/pip install -r requirements.txt;
	touch venv/bin/activate

venv2: virtualenv enos/venv/bin/activate
enos/venv/bin/activate: enos/requirements.txt
	cd enos; test -d venv || virtualenv --python=python2 venv
	cd enos; venv/bin/pip install -r requirements.txt;
	cd enos; touch venv/bin/activate

# This rule installs dependencies with pip in virtual environments (venv).
# `python3' and `python3-pip' have to be already installed. If `virtualenv'
# is not installed, this rule installs it with pip in the user directory.
# This rule also install EnOS dependencies if EnOS is here.
install_deps: venv3 venv2
	
remove_deps:
	-if [ -d "enos" ]; then cd enos; rm -rf venv; fi
	-rm -rf venv

package: mad.tgz
mad.tgz: $(PACKAGE_TARGETS)
	tar -zcvf $@\
		--exclude "enos/current*"\
		--exclude "enos/venv"\
		--exclude "enos/.vagrant"\
		--exclude "enos/enos_*"\
		--exclude "venv"\
		--exclude "__pycache__"\
		--exclude "*.swo"\
		--exclude "*.swp" $^

small_package: small_mad.tgz
small_mad.tgz: $(PACKAGE_TARGETS)
	tar -zcvf $@\
		--exclude "bench_params.yaml"\
		--exclude "/enos/venv"\
		--exclude "enos/reservation*"\
		--exclude "enos/.vagrant"\
		--exclude "enos/enos_*"\
		--exclude "venv"\
		--exclude "__pycache__"\
		--exclude "*.swo"\
		--exclude "*.swp" $^

backup:
	# eval is used here to set the following variables inside the rule:
	$(eval TIMESTAMP := $(shell date +%Y-%m-%d_%H:%M:%S))
	$(eval ARCHIVE_DIR := $(shell readlink -f "./expes/"))
	$(eval RUN_DIR := "$(ARCHIVE_DIR)/$(TIMESTAMP)")
	mkdir -p $(RUN_DIR)
	# The "-" at the beginning of the following lines is used to ignore
	# potentiall errors:
	-mv result*.txt $(RUN_DIR)
	-mv result*.html $(RUN_DIR)
	-cp -Lr enos/current $(RUN_DIR)
	-mv MadBench_* $(RUN_DIR)

tgz_backup:
	# eval is used here to set the following variables inside the rule:
	$(eval TIMESTAMP := $(shell date +%Y-%m-%d_%H:%M:%S))
	$(eval ARCHIVE_DIR := $(shell readlink -f "./expes/"))
	$(eval RUN_DIR := "$(ARCHIVE_DIR)/$(TIMESTAMP)")
	mkdir -p $(RUN_DIR)
	# The "-" at the beginning of the following lines is used to ignore
	# potentiall errors:
	-mv result_*.txt $(RUN_DIR)
	-mv result_*.html $(RUN_DIR)
	-mv fail_* $(RUN_DIR)
	-mv MadBench_* $(RUN_DIR)
	-cp -Lr enos/current $(RUN_DIR)
	-cd $(ARCHIVE_DIR); tar --force-local -cvzf $(TIMESTAMP).tgz $(TIMESTAMP)
	@echo $(RUN_DIR).tgz

tgz_snapshot:
	# eval is used here to set the following variables inside the rule:
	$(eval TIMESTAMP := $(shell date +%Y-%m-%d_%H:%M:%S))
	$(eval ARCHIVE_DIR := $(shell readlink -f "./expes/"))
	$(eval RUN_DIR := "$(ARCHIVE_DIR)/$(TIMESTAMP)")
	mkdir -p $(RUN_DIR)
	# The "-" at the beginning of the following lines is used to ignore
	# potentiall errors:
	-cp result_*.txt $(RUN_DIR)
	-cp result_*.html $(RUN_DIR)
	-cp fail_* $(RUN_DIR)
	-cp -r MadBench_* $(RUN_DIR)
	-cp -Lr enos/current $(RUN_DIR)
	-cd $(ARCHIVE_DIR); tar --force-local -cvzf $(TIMESTAMP).tgz $(TIMESTAMP)
	@echo $(RUN_DIR).tgz

tags: CTAGS-exists
	# exuberant-ctags must be installed
#	ifndef CTAGS
#		$(error "'ctags' is not available, exuberant-ctags must be installed.")
#	endif
	ctags -R --fields=+l --languages=python --python-kinds=-iv \
		--exclude=venv

clean:
	-rm result_*.txt $(RUN_DIR)
	-rm result_*.html $(RUN_DIR)
	-rm fail_* $(RUN_DIR)
	-rm -rf MadBench_*

mr_clean: clean
	-rm -rf enos/enos_* enos/current

