# -*- coding: utf-8 -*-

import os, sys
import logging
from subprocess import run
from collections import namedtuple
from utils.constants import (ENOS_DIR, OPENSTACK_DIR)
from utils.errors import (MadFailedHostsError, MadUnreachableHostsError,
                          MadProviderMissingConfigurationKeys,
                          MadFilePathError)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def listify(obj):
    """ Returns a list from `obj`. """
    return obj if isinstance(obj, list) or obj is None else [obj]

def printerr(str):
    """
    Print to stderr (unbuferred) instead of stdout (buffered) -- better with
    threaded code!.
    """
    print(str, file=sys.stderr)

def run_ansible(inventory=None, config_vars=[], playbook=None):
    if not inventory:
        inventory = os.path.join(ENOS_DIR, "multinode")

    #if not config_vars:
    vars_file = ENOS_DIR + "kolla/ansible/group_vars/all.yml"
    global_file = ENOS_DIR + "globals.yml"
    pass_file = ENOS_DIR + "passwords.yml"

    config_vars.append("@" + vars_file)
    config_vars.append("@" + global_file)
    config_vars.append("@" + pass_file)
    config_vars.append("action=deploy")

    if not playbook:
        print("No playbook has been provided - exit the program :(")
        sys.exit(1)

    # Forge the desired call to Ansible:
    cmd = ["ansible-playbook"]
    cmd.extend(("-i", inventory))
    for config_var in config_vars:
        cmd.extend(("-e", config_var))
    cmd.append(playbook)

    # Call ansible and return its return code
    return run(cmd).returncode

