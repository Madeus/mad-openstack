# -*- coding: utf-8 -

import os

MAD_PATH = os.path.abspath(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
ENOS_DIR = os.path.join(MAD_PATH, "enos/current/")
OPENSTACK_DIR= os.path.join(MAD_PATH, "components/openstack/")

