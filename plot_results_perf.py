#!/usr/bin/env python

import yaml
import os, sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
from scipy import stats
import matplotlib.pyplot as plt

# TO ADAPT TO YOUR NEEDS:
ttypes = ('dag_2t', 'dag_nt4')

def mean (list):
    if len(list) != 0:
        return sum(list)/len(list)
    else:
        return 0

if __name__ == "__main__":

    fname = sys.argv[1]
    with open(fname, "r") as f:
        results = yaml.load(f)

    arranged_results = {
       test_type:
            { registry:
                [ result["time"]
                    for result in results
                    if (result["params"]["registry"] == registry
                        and result["params"]["test_type"] == test_type
                            and result["failure"] == 0) ]
              for registry in
              [ result["params"]["registry"] for result in results ] }
        for test_type in
        [ result["params"]["test_type"] for result in results ] }

    #registries = ('local', 'remote')
    #ttypes = ('seq_1t', 'fj_1t', 'dag_1t', 'dag_2t', 'dag_nt')
    #ttypes = ('seq_1t', 'dag_2t', 'dag_nt')
    # Extract test types and registry types from the result file:
    #ttypes = tuple(arranged_results.keys())
    registries = tuple(arranged_results[ttypes[0]].keys())

    arranged_means = {}
    arranged_stds = {}

    # Compute means and standard deviations: 
    for registry in registries:
        arranged_means[registry] = {}
        arranged_stds[registry] = {}
        for ttype, tres in arranged_results.items():
            arranged_means[registry][ttype] = mean(tres[registry])
            arranged_stds[registry][ttype] = np.std(tres[registry])

    # Compute max values:
    max_values = {}
    for registry in registries:
        max_values[registry] = max(arranged_means[registry].values())

    # Populate appropriate structures for matplotlib:
    means={}
    stds={}
    for registry in registries:
        means[registry]=[]
        stds[registry]=[]
        for ttype in ttypes:
            means[registry].append(arranged_means[registry][ttype])
            stds[registry].append(arranged_stds[registry][ttype])

    fig, ax = plt.subplots()

    index = np.arange(len(ttypes))
    bar_width = 0.35/len(registries)*2

    opacity = 0.5
    error_config = {'ecolor': 'black', 'alpha': 0.8}


    rects = {}
    colors = ['b', 'r', 'g']
    i=0

    for registry in registries:
        rects[registry] = ax.bar(index + i*bar_width, means[registry], bar_width,
                            alpha=opacity, color=colors[i],
                            yerr=stds[registry], error_kw=error_config,
                            label=registry)
        i=i+1
        for rect in rects[registry]:
            height = rect.get_height()
            if height == max_values[registry]:
                txt = 1.0
            else: 
                txt = float(height/max_values[registry])
            plt.text(rect.get_x() + rect.get_width()/2.0, height*1.02,
                    '%.2f' % txt, color='gray', ha='center', va='bottom')

    ax.set_xlabel('Tests')
    ax.set_ylabel('Time (s)')
    ax.set_title('Time to deploy OpenStack for different tests')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(ttypes)
    ax.legend()

    fig.tight_layout()
    plt.savefig(os.path.splitext(fname)[0]+'.svg')

