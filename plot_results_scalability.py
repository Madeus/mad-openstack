#!/usr/bin/env python

import yaml
import os, sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
from scipy import stats
import matplotlib.pyplot as plt

def mean (list):
    if len(list) != 0:
        return sum(list)/len(list)
    else:
        return 0

if __name__ == "__main__":

    fname = sys.argv[1]
    with open(fname, "r") as f:
        results = yaml.load(f)

    arranged_results = {
       compute_node:
            { ttype:
                [ result["time"]
                    for result in results
                    if (result["params"]["test_type"] == ttype
                        and result["params"]["compute_nodes"] == compute_node
                            and result["failure"] == 0) ]
              for ttype in
              [ result["params"]["test_type"] for result in results ] }
        for compute_node in
        [ result["params"]["compute_nodes"] for result in results ] }

    #registries = ('local', 'remote')
    #ttypes = ('seq_1t', 'fj_1t', 'dag_1t', 'dag_2t', 'dag_nt')
    ttypes = ('seq_1t', 'fj_1t', 'dag_1t', 'dag_nt')
    #ttypes = ('seq_1t', 'dag_2t', 'dag_nt')
    # Extract test types and registry types from the result file:
    #ttypes = tuple(arranged_results.keys())
    #compute_nodes = tuple(arranged_results.keys())
    compute_nodes = set(arranged_results.keys())

    arranged_means = {}
    arranged_stds = {}

    # Compute means and standard deviations: 
    for ttype in ttypes:
        arranged_means[ttype] = {}
        arranged_stds[ttype] = {}
        for compute_node, tres in arranged_results.items():
            arranged_means[ttype][compute_node] = mean(tres[ttype])
            arranged_stds[ttype][compute_node] = np.std(tres[ttype])

    # Compute max values:
    max_values = {}
    for ttype in ttypes:
        max_values[ttype] = max(arranged_means[ttype].values())

    # Populate appropriate structures for matplotlib:
    means={}
    stds={}
    for ttype in ttypes:
        means[ttype]=[]
        stds[ttype]=[]
        for compute_node in compute_nodes:
            means[ttype].append(arranged_means[ttype][compute_node])
            stds[ttype].append(arranged_stds[ttype][compute_node])

    fig, ax = plt.subplots()

    index = np.arange(len(compute_nodes))
    bar_width = 0.2

    opacity = 0.5
    error_config = {'ecolor': 'black', 'alpha': 0.8}


    rects = {}
    colors = ['b', 'r', 'g', 'y']
    i=0

    for ttype in ttypes:
        rects[ttype] = ax.bar(index + i*bar_width, means[ttype], bar_width,
                            alpha=opacity, color=colors[i],
                            yerr=stds[ttype], error_kw=error_config,
                            label=ttype)
        i=i+1
        for rect in rects[ttype]:
            height = rect.get_height()
            if height == max_values[ttype]:
                txt = 1.0
            else: 
                txt = float(height/max_values[ttype])
            plt.text(rect.get_x() + rect.get_width()/2.0, height*1.02,
                    '%.2f' % txt, color='gray', ha='center', va='bottom')

    ax.set_xlabel('Tests')
    ax.set_ylabel('Time (s)')
    ax.set_title('Time to deploy OpenStack by increasing the number of compute nodes')
    ax.set_xticks(index + bar_width)
    ax.set_xticklabels(compute_nodes)
    ax.legend()

    fig.tight_layout()
    plt.savefig(os.path.splitext(fname)[0]+'.svg')

