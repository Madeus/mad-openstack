#!/usr/bin/env python3

from components.openstack.facts import Facts
from components.openstack.common import Common
from components.openstack.haproxy import HAProxy
from components.openstack.memcached import MemCached
from components.openstack.rabbitmq import RabbitMQ
from components.openstack.mariadb import MariaDB
from components.openstack.keystone import Keystone
from components.openstack.glance import Glance
from components.openstack.nova import Nova
from components.openstack.openvswitch import OpenVSwitch
from components.openstack.neutron import Neutron
from assembly import Assembly


# Initialize our OpenStack components:
fac = Facts()
com = Common()
ha = HAProxy()
mem = MemCached()
rab = RabbitMQ()
mar = MariaDB()
key = Keystone()
gla = Glance()
nov = Nova()
ovs = OpenVSwitch()
neu = Neutron()

# Initialize an assembly to manage them:
assembly = Assembly([
    [fac, 'facts'],
    [com, 'common'],
    [ha, 'haproxy'],
    [mem, 'memcached'],
    [rab, 'rabbitmq'],
    [mar, 'mariadb'],
    [key, 'keystone'],
    [gla, 'glance'],
    [nov, 'nova'],
    [ovs, 'openvswitch'],
    [neu, 'neutron']])

for service in [fac, com, ha, mem, rab, mar, key, gla, nov, ovs, neu]:
    service.net.initialize()

# Connects our components
assembly.auto_connect('facts', 'common')
assembly.auto_connect('facts', 'haproxy')
assembly.auto_connect('facts', 'rabbitmq')
assembly.auto_connect('facts', 'memcached')
assembly.auto_connect('facts', 'openvswitch')

assembly.auto_connect('facts', 'nova')
assembly.auto_connect('facts', 'neutron')
assembly.auto_connect('facts', 'glance')

assembly.auto_connect('common', 'mariadb')
assembly.auto_connect('haproxy', 'mariadb')

assembly.auto_connect('mariadb', 'keystone')
assembly.auto_connect('mariadb', 'nova')
assembly.auto_connect('mariadb', 'glance')
assembly.auto_connect('mariadb', 'neutron')

# I used to set:
# assembly.auto_connect('keystone', 'nova/glance/neutron')
# but since they all share mariadb ports, they try to add it naively
key.net.ports['keystone'].connect(nov.net.ports['keystone'])
key.net.ports['keystone'].connect(gla.net.ports['keystone'])
key.net.ports['keystone'].connect(neu.net.ports['keystone'])

# Fire the deployment process:
assembly.auto_run()

