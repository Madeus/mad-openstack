
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, MAD_PATH)
from subprocess import run

OPENSTACK_DIR = os.path.join(MAD_PATH, "components/openstack_dag_nt5/")
PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/nova/")


class Nova(object):
    """ Define a new component of type Nova. """

    # Define the different places of the component
    places = ['initiated', 'pulled', 'ready', 'restarted', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'pull', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'config', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'register', 'source': 'initiated', 'dest': 'deployed'},
            {'name': 'create_db', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'upgrade_db', 'source': 'pulled', 'dest': 'ready'},
            {'name': 'upgrade_api_db', 'source': 'pulled', 'dest': 'ready'},
            {'name': 'restart', 'source': 'ready', 'dest': 'restarted'},
            {'name': 'simple_cell_setup', 'source': 'restarted', 'dest': 'deployed'},
    ]

    ports = [
            #{'name': 'facts', 'inside_link': 'pull'},
            {'name': 'mariadb', 'inside_link': 'create_db'},
            {'name': 'keystone', 'inside_link': 'register'},
            {'name': 'nova', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_nova.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_simple_cell_setup(self):
        print("Setup a Nova simple cell")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_simple_cell_setup",
            self.playbook]).returncode

    def func_restart(self):
        print("Restart Nova components")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart",
            self.playbook]).returncode

    def func_upgrade_api_db(self):
        print("Upgrade Nova api databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_upgrade_api_db",
            self.playbook]).returncode

    def func_upgrade_db(self):
        print("Upgrade Nova databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_upgrade_db",
            self.playbook]).returncode

    def func_create_db(self):
        print("Create Nova databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_create_db",
            self.playbook]).returncode

    def func_register(self):
        print("Register the component Nova to Keystone")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_register",
            self.playbook]).returncode

    def func_config(self):
        print("Configure the component Nova")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_config",
            self.playbook]).returncode

    def func_pull(self):
        print("Pull the component Nova")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=pull",
            self.playbook]).returncode

    def nova(self):
        return self.net.get_place('deployed').state

