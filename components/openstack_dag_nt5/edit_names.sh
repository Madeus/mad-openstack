#!/bin/bash

# add the following to rabbitmq extra_vars:
# extra_vars.update({
#        'role_rabbitmq_cluster_cookie': '{{ rabbitmq_cluster_cookie }}',
#        'role_rabbitmq_cluster_port': '{{ rabbitmq_cluster_port }}',
#        'role_rabbitmq_epmd_port': '{{ rabbitmq_epmd_port }}',
#        'role_rabbitmq_groups': 'rabbitmq',
#        'role_rabbitmq_management_port': '{{ rabbitmq_management_port }}',
#        'role_rabbitmq_password': '{{ rabbitmq_password }}',
#        'role_rabbitmq_port': '{{ rabbitmq_port }}',
#        'role_rabbitmq_user': '{{ rabbitmq_user }}',
#    })


for i in haproxy memcached rabbitmq keystone glance nova neutron
do
    cp mariadb.py ${i}.py;
    sed -i "s/mariadb/${i}/" ${i}.py;
    cp ansible/site_mariadb.yml ansible/site_${i}.yml;
    sed -i "s/mariadb/${i}/" ansible/site_${i}.yml;
done

sed -i 's/MariaDB/HAProxy/' haproxy.py
sed -i 's/MariaDB/MemCached/' memcached.py
sed -i 's/MariaDB/RabbitMQ/' rabbitmq.py
sed -i 's/MariaDB/Keystone/' keystone.py
sed -i 's/MariaDB/Glance/' glance.py
sed -i 's/MariaDB/Nova/' nova.py
sed -i 's/MariaDB/Neutron/' neutron.py
