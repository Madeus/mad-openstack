
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, MAD_PATH)
from subprocess import run

OPENSTACK_DIR = os.path.join(MAD_PATH, "components/openstack_dag_nt5/")
PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/neutron/")



class Neutron(object):
    """ Define a new component of type Neutron. """

    # Define the different places of the component
    places = ['initiated', 'pulled', 'ready', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'pull', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'config', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'register', 'source': 'initiated', 'dest': 'deployed'},
            {'name': 'create_db', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'upgrade_db', 'source': 'pulled', 'dest': 'ready'},
            {'name': 'upgrade_db_fwaas', 'source': 'pulled', 'dest': 'ready'},
            {'name': 'restart', 'source': 'ready', 'dest': 'deployed'},
            #{'name': 'deploy', 'source': 'pulled', 'dest': 'deployed'},
    ]

    ports = [
            #{'name': 'facts', 'inside_link': 'pull'},
            {'name': 'mariadb', 'inside_link': 'create_db'},
            {'name': 'keystone', 'inside_link': 'register'},
            {'name': 'neutron', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_neutron.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_restart(self):
        print("Restart Neutron containers")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart",
            self.playbook]).returncode

    def func_upgrade_db_fwaas(self):
        print("Upgrade Neutron FWaaS databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_upgrade_db_fwaas",
            self.playbook]).returncode

    def func_upgrade_db(self):
        print("Upgrade Neutron databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_upgrade_db",
            self.playbook]).returncode

    def func_create_db(self):
        print("Create Neutron databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_create_db",
            self.playbook]).returncode

    def func_register(self):
        print("Register the component Neutron to Keystone")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_register",
            self.playbook]).returncode

    def func_config(self):
        print("Configure the component Neutron")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_config",
            self.playbook]).returncode

    def func_pull(self):
        print("Pull the component Neutron")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=pull",
            self.playbook]).returncode

    def neutron(self):
        return self.net.get_place('deployed').state

