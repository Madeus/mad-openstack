
import os, yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import OPENSTACK_DIR

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/facts/")


class Facts(object):
    """ Define a new component of type Facts. """

    # Define the different places of the component
    places = ['initiated', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'deploy', 'source': 'initiated', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'facts', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_facts.yml')

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component Facts")
        return run_ansible(playbook=self.playbook)

    def facts(self):
        return self.net.get_place('deployed').state

