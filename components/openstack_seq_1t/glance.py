
import os, yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import OPENSTACK_DIR

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/glance/")


class Glance(object):
    """ Define a new component of type Glance. """

    # Define the different places of the component
    places = ['initiated', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'deploy', 'source': 'initiated', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'keystone', 'inside_link': 'deploy'},
            {'name': 'glance', 'inside_link': 'deployed'},
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_glance.yml')

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component Glance")
        return run_ansible(playbook=self.playbook)

    def glance(self):
        return self.net.get_place('deployed').state

