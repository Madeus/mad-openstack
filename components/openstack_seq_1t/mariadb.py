
import os, yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import OPENSTACK_DIR

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/mariadb/")


class MariaDB(object):
    """ Define a new component of type MariaDB. """

    # Define the different places of the component
    places = ['initiated', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'deploy', 'source': 'initiated', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'memcached', 'inside_link': 'deploy'},
            {'name': 'mariadb', 'inside_link': 'deployed'},
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_mariadb.yml')

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component MariaDB")
        return run_ansible(playbook=self.playbook)

    def mariadb(self):
        return self.net.get_place('deployed').state

