
import os, yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, OPENSTACK_DIR)

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/rabbitmq/")


class RabbitMQ(object):
    """ Define a new component of type RabbitMQ. """

    # Define the different places of the component
    places = ['initiated', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'deploy', 'source': 'initiated', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'mariadb', 'inside_link': 'deploy'},
            {'name': 'rabbitmq', 'inside_link': 'deployed'},
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_rabbitmq.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component RabbitMQ")
        return run_ansible(config_vars=["@rabbit_vars.yml"],
                playbook=self.playbook)

    def rabbitmq(self):
        return self.net.get_place('deployed').state

