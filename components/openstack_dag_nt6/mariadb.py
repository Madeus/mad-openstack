
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, MAD_PATH)
from subprocess import run

OPENSTACK_DIR = os.path.join(MAD_PATH, "components/openstack_dag_nt4/")
PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/mariadb/")


class MariaDB(object):
    """ Define a new component of type MariaDB. """

    # Define the different places of the component
    places = ['initiated', 'bootstrapped', 'restarted', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'pull', 'source': 'initiated', 'dest': 'bootstrapped'},
            {'name': 'bootstrap', 'source': 'initiated', 'dest': 'bootstrapped'},
            {'name': 'restart', 'source': 'bootstrapped', 'dest': 'restarted'},
            {'name': 'register', 'source': 'restarted', 'dest': 'deployed'},
            {'name': 'check', 'source': 'restarted', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'common', 'inside_link': 'register'},
            {'name': 'haproxy', 'inside_link': 'check'},
            {'name': 'mariadb', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_mariadb.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_check(self):
        print("Check that MariaDB is reachable from HAProxy")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_check",
            self.playbook]).returncode

    def func_register(self):
        print("Create HAProxy user in MariaDB")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_register",
            self.playbook]).returncode

    def func_restart(self):
        print("Restart MariaDB containers")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart",
            self.playbook]).returncode

    def func_bootstrap(self):
        print("Bootstrap the MariaDB component")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_bootstrap",
            self.playbook]).returncode

    def func_pull(self):
        print("Pull the component MariaDB")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=pull",
            self.playbook]).returncode

    def mariadb(self):
        return self.net.get_place('deployed').state

