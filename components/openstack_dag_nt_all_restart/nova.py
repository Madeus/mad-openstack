
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, MAD_PATH)
from subprocess import run

OPENSTACK_DIR= os.path.join(MAD_PATH, "components/openstack_dag_nt2/")
PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/nova/")

class Nova(object):
    """ Define a new component of type Nova. """

    # Define the different places of the component
    places = ['initiated', 'bootstrapped', 'db_ready',
            'restarted', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'pull',
                'source': 'initiated',
                'dest': 'bootstrapped'},
            {'name': 'config',
                'source': 'initiated',
                'dest': 'bootstrapped'},
            {'name': 'register',
                'source': 'initiated',
                'dest': 'bootstrapped'},
            {'name': 'create_db',
                'source': 'initiated',
                'dest': 'bootstrapped'},
            {'name': 'upgrade_db',
                'source': 'bootstrapped',
                'dest': 'db_ready'},
            {'name': 'restart',
                'source': 'db_ready',
                'dest': 'restarted'},
            {'name': 'simple_cell_setup',
                'source': 'restarted',
                'dest': 'deployed'},
    ]

    ports = [
            #{'name': 'facts', 'inside_link': 'pull'},
            {'name': 'mariadb', 'inside_link': 'create_db'},
            {'name': 'keystone', 'inside_link': 'register'},
            {'name': 'nova', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_nova.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_simple_cell_setup(self):
        print("Setup simple cell for Nova")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_simple_cell_setup",
            self.playbook]).returncode

    def func_restart(self):
        print("Restart nova containers")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart",
            self.playbook]).returncode

    def func_restart_nova_placement(self):
        print("Restart nova_placement")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart_nova_placement",
            self.playbook]).returncode

    def func_restart_nova_libvirt(self):
        print("Restart nova_libvirt")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart_nova_libvirt",
            self.playbook]).returncode

    def func_restart_nova_ssh(self):
        print("Restart nova_ssh")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart_nova_ssh",
            self.playbook]).returncode

    def func_restart_nova_compute(self):
        print("Restart nova_compute")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart_nova_compute",
            self.playbook]).returncode

    def func_restart_nova_api(self):
        print("Restart nova_api")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_restart_nova_api",
            self.playbook]).returncode

    def func_upgrade_db(self):
        print("Upgrade Nova's databases to the latest scheme")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_bootstrap_service",
            self.playbook]).returncode

    def func_create_db(self):
        print("Create Nova's databases")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_bootstrap",
            self.playbook]).returncode

    def func_register(self):
        print("Register the component Nova to Keystone")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_register",
            self.playbook]).returncode

    def func_config(self):
        print("Configure the component Nova")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_config",
            self.playbook]).returncode

    def func_pull(self):
        print("Pull the component Nova")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=pull",
            self.playbook]).returncode

    def nova(self):
        return self.net.get_place('deployed').state

