
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, OPENSTACK_DIR)
from subprocess import run

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/common/")

class Common(object):
    """ Define a new component of type Common. """

    # Define the different places of the component
    places = ['initiated', 'deployed', 'ktb_deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'ktb_deploy', 'source': 'initiated', 'dest': 'ktb_deployed'},
            {'name': 'deploy', 'source': 'ktb_deployed', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'facts', 'inside_link': 'ktb_deploy'},
            {'name': 'common', 'inside_link': 'ktb_deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_common.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component Common")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=oth_deploy",
            self.playbook]).returncode

    def func_ktb_deploy(self):
        print("Deploy the component Common_KTB")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=ktb_deploy",
            self.playbook]).returncode

    def common(self):
        return self.net.get_place('ktb_deployed').state

