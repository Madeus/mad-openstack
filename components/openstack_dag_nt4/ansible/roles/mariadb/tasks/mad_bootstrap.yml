---
- name: Ensuring config directories exist
  file:
    path: "{{ node_config_directory }}/{{ item.key }}"
    state: "directory"
    recurse: yes
  when:
    - inventory_hostname in groups[item.value.group]
    - item.value.enabled | bool
  with_dict: "{{ mariadb_services }}"

- name: Copying over config.json files for services
  vars:
    service_name: "mariadb"
    service: "{{ mariadb_services[service_name] }}"
  template:
    src: "{{ service_name }}.json.j2"
    dest: "{{ node_config_directory }}/{{ service_name }}/config.json"
  register: mariadb_config_json
  when:
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Copying over galera.cnf
  vars:
    service_name: "mariadb"
    service: "{{ mariadb_services[service_name] }}"
  merge_configs:
    sources:
      - "{{ role_path }}/templates/galera.cnf.j2"
      - "{{ node_custom_config }}/galera.cnf"
      - "{{ node_custom_config }}/mariadb/{{ inventory_hostname }}/galera.cnf"
    dest: "{{ node_config_directory }}/{{ service_name }}/galera.cnf"
  register: mariadb_galera_conf
  when:
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Copying over wsrep-notify.sh
  template:
    src: "{{ role_path }}/templates/wsrep-notify.sh.j2"
    dest: "{{ node_config_directory }}/{{ item.key }}/wsrep-notify.sh"
  register: mariadb_wsrep_notify
  when:
    - inventory_hostname in groups[item.value.group]
    - item.value.enabled | bool
  with_dict: "{{ mariadb_services }}"

- name: Check mariadb containers
  kolla_docker:
    action: "compare_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ item.value.container_name }}"
    image: "{{ item.value.image }}"
    volumes: "{{ item.value.volumes }}"
  register: check_mariadb_containers
  when:
    - action != "config"
    - inventory_hostname in groups[item.value.group]
    - item.value.enabled | bool
  with_dict: "{{ mariadb_services }}"

- name: Cleaning up temp file on localhost
  local_action: file path=/tmp/kolla_mariadb_cluster state=absent
  changed_when: False
  check_mode: no
  run_once: True

- name: Creating temp file on localhost
  local_action: copy content='' dest=/tmp/kolla_mariadb_cluster mode=0644
  changed_when: False
  check_mode: no
  run_once: True

- name: Creating mariadb volume
  kolla_docker:
    action: "create_volume"
    common_options: "{{ docker_common_options }}"
    name: "mariadb"
  register: mariadb_volume

- name: Writing hostname of host with existing cluster files to temp file
  local_action: copy content={{ ansible_hostname }} dest=/tmp/kolla_mariadb_cluster mode=0644
  changed_when: False
  check_mode: no
  when: not mariadb_volume | changed

- name: Registering host from temp file
  set_fact:
    has_cluster: "{{ lookup('file', '/tmp/kolla_mariadb_cluster') | length > 0 }}"

- name: Cleaning up temp file on localhost
  local_action: file path=/tmp/kolla_mariadb_cluster state=absent
  changed_when: False
  check_mode: no
  run_once: True

- name: Running MariaDB bootstrap container
  kolla_docker:
    action: "start_container"
    common_options: "{{ docker_common_options }}"
    detach: False
    environment:
      KOLLA_BOOTSTRAP:
      KOLLA_CONFIG_STRATEGY: "{{ config_strategy }}"
      DB_ROOT_PASSWORD: "{{ database_password }}"
      DB_MAX_TIMEOUT: "{{ database_max_timeout }}"
    image: "{{ mariadb_image_full }}"
    labels:
      BOOTSTRAP:
    name: "bootstrap_mariadb"
    restart_policy: "never"
    volumes:
      - "{{ node_config_directory }}/mariadb/:{{ container_config_directory }}/:ro"
      - "/etc/localtime:/etc/localtime:ro"
      - "kolla_logs:/var/log/kolla/"
      - "mariadb:/var/lib/mysql"
  when:
    - not has_cluster | bool
    - inventory_hostname == groups['mariadb'][0]

- set_fact:
    bootstrap_host: "{{ inventory_hostname }}"
  when:
    - not has_cluster | bool
    - inventory_hostname == groups['mariadb'][0]

- include: recover_cluster.yml
  when: mariadb_recover | default(False)
