
import os
import yaml
from mad import PetriNet
from utils.extra import run_ansible
from utils.constants import (ENOS_DIR, OPENSTACK_DIR)
from subprocess import run

PLAYBOOK_DIR = os.path.join(OPENSTACK_DIR, "ansible/roles/glance/")

class Glance(object):
    """ Define a new component of type Glance. """

    # Define the different places of the component
    places = ['initiated', 'pulled', 'deployed']

    # Define the different transitions of the component
    transitions = [
            {'name': 'pull', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'config', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'register', 'source': 'initiated', 'dest': 'pulled'},
            {'name': 'deploy', 'source': 'pulled', 'dest': 'deployed'},
    ]

    ports = [
            {'name': 'facts', 'inside_link': 'pull'},
            {'name': 'mariadb', 'inside_link': 'config'},
            {'name': 'keystone', 'inside_link': 'register'},
            {'name': 'glance', 'inside_link': 'deployed'}
    ]

    def __init__(self, step=False):

        self.net = PetriNet(self, self.places, self.transitions, self.ports,
                initial='initiated')

        self.playbook = os.path.join(OPENSTACK_DIR, 'ansible/site_glance.yml')
        self.inventory = os.path.join(ENOS_DIR, 'multinode')

        # Add kolla variables
        self.vars_file=ENOS_DIR + "kolla/ansible/group_vars/all.yml"

        # Add globals.yml
        self.global_file=ENOS_DIR + "globals.yml"

        # Add passwords
        self.pass_file=ENOS_DIR + "passwords.yml"

    # Default callbacks can be defined with the name 'func_<transition_name>'
    def func_deploy(self):
        print("Deploy the component Glance")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_deploy",
            self.playbook]).returncode

    def func_register(self):
        print("Register the component Glance to Keystone")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_register",
            self.playbook]).returncode

    def func_config(self):
        print("Configure the component Glance")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=mad_config",
            self.playbook]).returncode

    def func_pull(self):
        print("Pull the component Glance")
        return run(["ansible-playbook",
            "-i", self.inventory,
            "-e", "@" + self.vars_file,
            "-e", "@" + self.global_file,
            "-e", "@" + self.pass_file,
            "-e", "action=pull",
            self.playbook]).returncode

    def glance(self):
        return self.net.get_place('deployed').state

