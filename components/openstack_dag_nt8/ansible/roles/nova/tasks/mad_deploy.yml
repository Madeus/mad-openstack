---
- name: Restart nova-ssh container
  vars:
    service_name: "nova-ssh"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    pid_mode: "{{ service.pid_mode | default('') }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-libvirt container
  vars:
    service_name: "nova-libvirt"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    pid_mode: "{{ service.pid_mode | default('') }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  register: restart_nova_libvirt
  # NOTE(Jeffrey4l): retry 5 to remove nova_libvirt container because when
  # guests running, nova_libvirt will raise error even though it is removed.
  retries: 5
  until: restart_nova_libvirt | success
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart placement-api container
  vars:
    service_name: "placement-api"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-api container
  vars:
    service_name: "nova-api"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-scheduler container
  vars:
    service_name: "nova-scheduler"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-conductor container
  vars:
    service_name: "nova-conductor"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool


- name: Restart nova-consoleauth container
  vars:
    service_name: "nova-consoleauth"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-novncproxy container
  vars:
    service_name: "nova-novncproxy"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-spicehtml5proxy container
  vars:
    service_name: "nova-spicehtml5proxy"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-serialproxy container
  vars:
    service_name: "nova-serialproxy"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-compute container
  vars:
    service_name: "nova-compute"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

- name: Restart nova-compute-ironic container
  vars:
    service_name: "nova-compute-ironic"
    service: "{{ nova_services[service_name] }}"
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    privileged: "{{ service.privileged | default(False) }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
  when:
    - action != "config"
    - inventory_hostname in groups[service.group]
    - service.enabled | bool

# nova-compute-fake is special. It will start multi numbers of container
# so put all variables here rather than defaults/main.yml file
- name: Restart nova-compute-fake containers
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "nova_compute_fake_{{ item }}"
    image: "{{ nova_compute_image_full }}"
    privileged: True
    volumes:
      - "{{ node_config_directory }}/nova-compute-fake-{{ item }}/:{{ container_config_directory }}/:ro"
      - "/etc/localtime:/etc/localtime:ro"
      - "/lib/modules:/lib/modules:ro"
      - "/run:/run:shared"
      - "kolla_logs:/var/log/kolla/"
  with_sequence: start=1 end={{ num_nova_fake_per_node }}
  when:
    - action != "config"
    - inventory_hostname in groups['compute']
    - enable_nova_fake | bool

- name: Create cell0 mappings
  command: >
    docker exec nova_api nova-manage cell_v2 map_cell0
  register: map_cell0
  changed_when:
    - map_cell0 | success
    - '"Cell0 is already setup" not in map_cell0.stdout'
  failed_when:
    - map_cell0.rc != 0
  run_once: True
  delegate_to: "{{ groups['nova-api'][0] }}"

- include: bootstrap_service.yml
  when: map_cell0.changed

- name: Create base cell for legacy instances
  command: >
    docker exec nova_api nova-manage cell_v2 create_cell
  register: base_cell
  changed_when:
    - base_cell | success
  failed_when:
    - base_cell.rc != 0
    - '"already exists" not in base_cell.stdout'
  run_once: True
  delegate_to: "{{ groups['nova-api'][0] }}"

- name: Waiting for nova-compute service up
  command: >
    docker exec kolla_toolbox openstack
    --os-interface internal
    --os-auth-url {{ keystone_admin_url }}
    --os-identity-api-version 3
    --os-project-domain-name {{ openstack_auth.domain_name }}
    --os-tenant-name {{ openstack_auth.project_name }}
    --os-username {{ openstack_auth.username }}
    --os-password {{ keystone_admin_password }}
    --os-user-domain-name {{ openstack_auth.domain_name }}
    compute service list -f json --service nova-compute
  register: nova_compute_services
  changed_when: false
  run_once: True
  delegate_to: "{{ groups['nova-api'][0] }}"
  retries: 20
  delay: 10
  until:
    - nova_compute_services | success
    - nova_compute_services.stdout | from_json | length != 0

- name: Discovering nova hosts
  command: >
    docker exec nova_api nova-manage cell_v2 discover_hosts
  register: discover_hosts
  changed_when: False
  run_once: True
  delegate_to: "{{ groups['nova-api'][0] }}"
