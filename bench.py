#!/usr/bin/env python3

# Execo jobs can be tagged as 'todo', 'skipped', 'done', or 'cancel'. Most of
# the times, failing experiments are due to G5K (i.e. a node is temporary
# unavailable). As a consequence, we retry failing tests.

import os, sys, yaml
import time as timer
from pprint import pformat
from subprocess import run
from execo import *
from execo_g5k import *
from execo_engine import *


# Define paths to load mad and enos virtual environments:
env_paths = {
        "mad": "/venv/bin/activate_this.py",
        "enos": "/enos//venv/bin/activate_this.py"
}


class MadBench(Engine):
    """An Execo engine to bench OpenStack deployment with MAD."""

    def __init__(self):
        self.dry_run = False
        self.monitoring = False
        self.reservation_file = None
        self.enos_config = None
        self.registry = None

        self.python2_bin = os.path.abspath(os.getcwd() + "/enos/venv/bin/python")
        self.python3_bin = os.path.abspath(os.getcwd() + "/venv/bin/python")

        super(MadBench, self).__init__()
        # Here we define the program's arguments:
        self.args_parser.add_argument(
                '-i', '--iterations',
                dest='iterations',
                help='Set the number of iterations.')
        self.args_parser.add_argument(
                '-f', '--benchmark-file',
                dest='config_file',
                help='Set the benchmark configuration file.')
        self.args_parser.add_argument(
                '-m', '--monitoring',
                dest='monitoring',
                action="store_true",
                default="False",
                help='Set EnOS monitoring stack.')
        self.args_parser.add_argument(
                '-n', '--dry-run',
                dest='dry_run',
                default = 0,
                choices=[0, 1, 2, 3],
                type=int,
                help='Can be used to fire transitions in different ways: \
                        (0) normal run (default); \
                        (1) with threads and fake ssh; \
                        (2) with threads but no ssh; \
                        (3) without threads nor ssh.')
        self.args_parser.add_argument(
                '-r', '--reservation-file',
                dest='reservation_file',
                help='Set the EnOS cnfiguration file.')
        self.args_parser.add_argument(
                '--registry',
                dest='registry',
                choices=['cached', 'local', 'remote'],
                help='Set the registry mode.')
        self.args_parser.add_argument(
                '-t', '--test',
                dest='test',
                help='Set the test defined in the benchmark config file.')

        #TODO: implement
        #self.args_parser.add_argument('-v', '--verbose',
        #        dest='verbose', help='Increase verbosity.')

    def load_env(self, env):
        """
        EnOS is written in python2 while MAD is written in python3. EnOS and
        MAD has specific dependencies which are written for their respective
        python version (which are thus not compatible). To avoid conflicts we
        use virtual environments (virtualenv). To execute a python call in the
        appropriate environment, it is sufficient to call the python binary
        from the desired virtualenv.
        """

        if env not in env_paths:
            print("The following env: " + env + "is not defined - leaving the"
                    " program :(")
            sys.exit(1)
        activate_file = os.getcwd() + env_paths[env]
        exec(open(activate_file).read(), dict(__file__=activate_file))

    def run_enos(self, cmd):
        return run([self.python2_bin] + "-m enos.enos".split()
                + cmd.split()).returncode

    def create_paramsweeper(self):
        """Test all the clusters for several env."""

        # A test must be provided if a configuration file is given:
        if self.args.config_file and self.args.test is None:
            self.args_parser.error(
                    "'-f|--benchmark-file' requires '-t|--test'.")

        # Set the benchmark parameters given the following priorities:
        # 1. params given as args
        # 2. params given from benchmark definition file

        if self.args.config_file:
            # Else, fetch the params from the benchmark definition file.
            # Check if the configuration file exists:
            if not os.path.isfile(self.args.config_file):
                print("The configuration file: " + self.args.config_file +
                        " does not exist - leaving the program :(")
                sys.exit(1)

            with open(self.args.config_file, "r") as f:
                test_definitions = yaml.load(f)
            
            # Check if the desired benchmark is defined in the definition file:
            if self.args.test not in test_definitions:
                print("The following test: " + self.args.test + " is not "
                        "defined in the given benchmark definition file: "
                        + self.args.config_file + " - leaving the program :(")
                sys.exit(1)

            test_definition = test_definitions[self.args.test]

            params = test_definition['params']
            if 'monitoring' in test_definition:
                self.monitoring = test_definition['monitoring']
            if 'dry_run' in test_definition:
                self.dry_run = int(test_definition['dry_run'])

        # The args defined by the user must override existing ones:
        self.reservation_file = self.args.reservation_file or \
                test_definition.get("reservation_file")
        if not self.reservation_file:
            print("No reservation_file provided - leaving the program :(")
            sys.exit(1)
        self.reservation_file = os.path.abspath(self.reservation_file)

        if not os.path.isfile(self.reservation_file):
            print('The reservation file: ' + self.reservation_file + ' does \
                    not exist - leaving the program :(')
            sys.exit(1)

        # Fetch information from EnOS reservation file:
        with open(self.reservation_file, "r") as f:
            self.enos_config = yaml.load(f)

        self.registry = self.args.registry or \
                test_definition["params"].get("registry")
        if not self.registry:
            print("Warning, no registry configuration found - set as 'cached'")

        self.monitoring = self.args.monitoring or \
                test_definition.get("monitoring", False)

        self.dry_run = int(self.args.dry_run or \
                test_definition.get("dry_run", "0"))

        self.iterations = self.args.iterations or \
                test_definition.get("iterations")
        if self.iterations:
            params['repeat'] = tuple(range(0, self.iterations))

        # Set the maximum of desired compute nodes in EnOS configuration file
        # to deploy enough nodes for the benchmark:
        if 'compute_nodes' in params:
            run(["sed -i 's/compute:.*/compute: " +
                str(max(params['compute_nodes'])) +
                "/' " + str(self.reservation_file)], shell=True)

        if "shell" in params['tool']:
            if 'dag_2t' in params['test_type'] or 'dag_nt' in \
            params['test_type']:
                    print('The tests "dag_2t" or "dag_nt" are not supported \
                    with the "shell" tool - leaving the program :(')
                    sys.exit(1)

        logger.info('Defining parameters: %s', pformat(params))

        combs = sweep(params)
        sweeper = ParamSweeper(self.result_dir + "/sweeper", combs)

        return sweeper

    def filtr(self, combs):
        """Filter combs on a per-registry basis (set local first)."""
        return sorted(
                # Sort on a per-repeat basis first:
                sorted(combs, key=lambda r: r['repeat']),
                # Sort the result per registry:
                key=lambda t: 1 if t["registry"] == "local"
                    else ( 2 if t["registry"] == "cached"
                    else 3)
                )

    def merge(mad_config, enos_config):
        """This method is used to override the EnOS configuration."""
        if isinstance(mad_config, dict) and isinstance(enos_config, dict):
            for k, v in enos_config.items():
                if k not in mad_config:
                    mad_config[k] = v
        return mad_config

    def redeploy(self, registry):
        """
        Redeploy is called for each new registry test. It is meant to deploy a new
        topology (e.g. when registry is 'local' an extra node is required).
        """
        # Since we are going to use EnOS, we set its virtual env:
        self.load_env("enos")

        if self.dry_run in [0, 1]:

            # Update the EnOS configuration file according to the registry config:
            if registry == "local":
                print("Change " + str(self.reservation_file))
                # Remove any local registry in the configuration file:
                run(["sed -i '/disco\/registry/d' "
                    + str(self.reservation_file)], shell=True)
                # Remove small
                run(["sed -i '/small:/d' "
                    + str(self.reservation_file)], shell=True)

                # If we use Vagrant: deploy the registry in a "small" VM:
                if self.enos_config['provider']['type'] == "vagrant":
                    # Add small
                    run(["sed -i '/^    compute:/a\ \ small:' "
                        + str(self.reservation_file)], shell=True)
                    # Add a registry node in the configuration file:
                    run(["sed -i '/^  small:/a\ \ \ \ disco\/registry: 1' "
                        + str(self.reservation_file)], shell=True)
                else:
                    # Add a registry node in the configuration file:
                    run(["sed -i '/^    compute:/a\ \ \ \ disco\/registry: 1' "
                        + str(self.reservation_file)], shell=True)
                # Set the registry as 'internal' for EnOS:
                run(["sed -i '/^registry:/{ N; s/\(^registry:\\n  type:\)"
                    ".*/\\1 internal/ }' "
                    + str(self.reservation_file)], shell=True)
            else:
                # Remove small:
                run(["sed -i '/small:/d' "
                    + str(self.reservation_file)], shell=True)
                # Remove any local registry in the configuration file:
                run(["sed -i '/disco\/registry/d' "
                    + str(self.reservation_file)], shell=True)
                # Unset any registry:
                run(["sed -i '/^registry:/{ N; s/\(^registry:\\n  type:\)"
                    ".*/\\1 none/ }' "
                    + str(self.reservation_file)], shell=True)

            # Each time we run the script, we bootstrap the deployment env:
            #   1. Be sure the infrastructure is up;
            #   2. Destroy previously downloaded docker images;
            #   3. Download every Docker images.

            os.chdir("enos")
            failure = self.run_enos("up -f " + str(self.reservation_file))
                    #+ " --force-deploy")

            if failure:
                print("An issue happened: the infra is not up "
                        "- leave the program :(")
                sys.exit(1)

            # If monitoring is enabled: deploy collectd agent locally
            if self.monitoring:
                if self.run_enos("localhost"):
                    print("WARNING: while monitoring is enabled, "
                            " collectd has not be deployed locally.")

            # Destroy exising containers and images on every node:
            self.run_enos("destroy --include-images")

            # If registry is 'cached' or 'local', images must be downloaded:
            if registry == 'cached':
                self.run_enos("bootstrap --cached")
            elif registry == 'local':
                self.run_enos("bootstrap")

            os.chdir("..")

    def run(self):

        sweeper = self.create_paramsweeper()

        current_registry = None

        # Bench each combination. As mentioned, we observe sometimes temporary
        # failures due to G5K, thus, we reschedule failed iterations:
        while len(sweeper.get_remaining()) > 0:

            # Combinations are sorted regarding the registry configuration:
            comb = sweeper.get_next(filtr=self.filtr)
            logger.info('Treating combination %s', pformat(comb))

            if current_registry != comb["registry"]:
                # When a new type of registry is tested, redeployment is needed
                current_registry = comb["registry"]
                self.redeploy(comb["registry"])

            # Since we are going to use EnOS, we set its virtual env
            self.load_env("enos")
            os.chdir("enos")

            # Save current results
            if self.monitoring:
                self.run_enos("backup")

            # If registry is cached: only destroy containers
            if comb['registry'] == "cached":
                self.run_enos("destroy")
            # If registry is local: destroy containers + images on OS hosts
            elif comb['registry'] == "local":
                self.run_enos("destroy")
                self.run_enos("destroy --local")
            # If registry is remote: destroy containers + images on every hosts
            elif comb['registry'] == "remote":
                self.run_enos("destroy --include-images")

            # Update the number of compute nodes if required
            if 'compute_nodes' in comb:
                run(["sed -i 's/compute:.*/compute: " +
                    str(comb['compute_nodes']) + "/' " +
                    str(self.reservation_file)], shell=true)
                if self.run_enos("up -f " + str(self.reservation_file)):
                    print("An issue happened: the infra is not up - jump to "
                            "the next iteration.")
                    continue

            os.chdir("..")

            cmd = ""
            dry_run_suffix=""

            if self.dry_run:
                dry_run_suffix="-n " + str(self.dry_run)

            if comb['tool'] == "shell":
                deploy_script = os.getcwd() + "/deploy_openstack.sh"
                cmd = [deploy_script, "-t", comb['test_type']]
            elif comb['tool'] == "mad":
                deploy_script = os.getcwd() + "/deploy_openstack.py"
                cmd = [self.python3_bin, deploy_script, "-t", \
                        comb['test_type']]
                if dry_run_suffix:
                    cmd.append(dry_run_suffix)
            else:
                print("The following tool: " + comb['tool'] + "is not implemented.")
                sys.exit(1)

            # Since we are going to use mad, we set its virtual env
            self.load_env("mad")

            start = timer.perf_counter()
            failure = run(cmd).returncode
            time = timer.perf_counter() - start
            
            prefix = ""
            cn_suffix = ""
            if "compute_nodes" in comb:
                cn_suffix = "n-" + str(comb["compute_nodes"]) +  "_"
            suffix = comb['test_type'] + "_"\
                    + comb['tool'] + "_"\
                    + comb['registry'] + "_"\
                    + cn_suffix\
                    + str(comb['repeat'])

            if failure:
                # Archive the results in specific file and retry later the expe
                prefix = "fail_"
                sweeper.skip(comb)
            else:
                sweeper.done(comb)

            if comb['tool'] == "mad":
                run(["mv " "result_tasks.txt " + prefix + "result_" + \
                        suffix + "_tasks.txt"], shell=True)
                run(["mv " "result_components.txt " + prefix + "result_" + \
                        suffix + "_components.txt"], shell=True)
                run(["mv " "gantt.html " + prefix + "result_" +  \
                        suffix + "_gantt.html"], shell=True)

            result_file = self.result_dir + "/results"
            results = { "params": comb, "time": time, "failure": failure }
            with open(result_file, "a") as f:
                yaml.dump([results], f, width = 72)

            logger.info('%s/%s', sweeper.get_done(), sweeper.get_sweeps())

            # Forget the skipped combs and replace them as remaining:
            sweeper.reset()


if __name__ == '__main__':
    e = MadBench()
    e.start()
