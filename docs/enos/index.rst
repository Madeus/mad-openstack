.. _enos:

EnOS
===============================

.. attention ::

   This lab is all inclusive (including MAD and EnOS) at
   https://gitlab.inria.fr/Madeus/mad-openstack

EnOS is a holistic framework to conduct evaluations of different
OpenStack configurations in an easy and reproducible manner. In
particular, EnOS helps you in deploying real OpenStack instances on
different types of infrastructure (from virtual environments based on
VMs like Vagrant, to real large-scale testbeds composed of bare-metal
machines like Grid'5000), stressing it and getting feedback.

Many projects exist to deploy OpenStack (e.g.  OpenStack-Ansible,
OpenStack-Chef, OpenStack Kolla, Kubernetes, Juju). EnOS relies on
Kolla to deploy OpenStack. Kolla is an OpenStack project which deploys
OpenStack modules as Docker containers, using Ansible.

EnOS' workflow is the following:

1. EnOS Up: book, provision and bootstrap resources
   
   * install dependencies (Ansible, Docker)
   * install monitoring tools (cAdvisor, collectd, influxdb, grafana)
     
2. EnOS Deploy: deploy OpenStack (based on Kolla)
3. EnOS Bench: benchmark OpenStack
4. EnOS Backup: backup the collected metrics
5. EnOS Destroy: release resources

As MAD is used to deploy OpenStack in this reproducibility guide, MAD
does not entirey use EnOS but only a subpart of it: EnOS Up. In other
words, EnOS is used in this guide to provision resources on Grid'5000
only. One can note that this provisioning step could also be performed
by MAD. We have made this choice because we compare MAD to Kolla that
does not perform provisioining.

.. note ::

   The source code is available at
   https://github.com/BeyondTheClouds/enos

.. note ::

   The documentation is available at
   https://enos.readthedocs.io/en/stable/
