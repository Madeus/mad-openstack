.. mad documentation master file, created by
   sphinx-quickstart on Mon Jul 23 17:04:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MAD-OpenStack's documentation!
==========================================

This documentation indicates all the details needed to reproduce the
experiments conducted on the deployment of OpenStack by using MAD
(*Madeus Application Deployer*), the python implementation of the
Madeus model. This work has been funded by the Discovery project.

The lab makes use of *EnOS* to book the resources on Grid'5000, and
*MAD* to deploy OpenStack on those resources. As a result the source
code of the lab contains EnOS as well as the specific MAD version used
to conduct our experiments.

This documentation first offers details on Madeus and MAD (the
deployment model and its implementation). Then OpenStack (that we aim
to deploy in the lab) and EnOS (used to provision resources on
Grid'5000) are presented. Then, the lab is detailed.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    madeus/index
    openstack/index
    enos/index
    reproducibility/index
    results/index

Code
-------
    
.. attention ::

   The all-inclusive source code of the lab (including MAD and EnOS)
   is available at
   https://gitlab.inria.fr/Madeus/mad-openstack
    

License
-------

MAD and EnOS are distributed under a GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007


