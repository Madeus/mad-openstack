.. _results:

Results
===============================

Presentation of result files
------------------------------

Since we deploy the ~mad-node~ interactively, every file are stored in
your ``$HOME`` directory and should be available even when the
reservation is finished.

First, global information can be found about the benchmark. More particularly,
check the file ``/MadBench_<date>/results`` which contains the deployment time for
each test. This file is filled as the benchmark runs (so it should be empty at
the beginning). Also, the output of the benchmark tool is recorded in
``/MadBench_<date>/stdout+stderr``.

You can use a script to plot figures from the global result file by typing:

.. code-block:: console

   (venv) user@mad-node:~/mad$ ./plot_results_perf_nova.py MadBench_*/results
   
Depending on your test, you might need to adapt the plotting script.

Furthermore, as the benchmark runs, some output files should appear in the
current directory. These files contain detailed information used to better
understand the previous results about the experiment. Particularly you should
see three types of file:

* *result_<test>_<tool>_<registry>_<iteration>_components.txt*: time spent for
  each component;
* *result_<test>_<tool>_<registry>_<iteration>_tasks.txt*: time spent per task;
* *result_<test>_<tool>_<registry>_<iteration>_gantt.html*: Gantt diagram of the
  run.

Backup and restore
-----------------------

You can either move your results to a backup archive by calling:

.. code-block:: console

   (venv) user@mad-node:~/mad$: make tgz_backup
   
or copy them to an archive by calling:

.. code-block:: console

   (venv) user@mad-node:~/mad$: make tgz_snapshot

Both command should output the path to the archive. You can use this information
to easily copy the archive on your own computer or to share it with someone:

.. code-block:: console

   user@laptop:~$ scp lyon.g5k:/<path_from_the_output> ./archive.tgz
   user@laptop:~$ mkdir result_expe
   user@laptop:~$ tar --force-local -xvzf archive.tgz -C result_expe


