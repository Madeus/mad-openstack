.. _openstack:

OpenStack
===============================

.. hint ::

   Everything you want to know on OpenStack at
   https://www.openstack.org/

OpenStack overview
---------------------
   
OpenStack is the de-facto solution to manage infrastructures (i.e. compute,
network, storage resources). To that end, it provides management mechanisms as a
modular platform composed of several projects, each of which is in charge of an
aspect of the infrastructure management. Among the various projects (30+), here
is a selection corresponding to the bare necessities to operate infrastructure:

* /nova/: the compute resource manager (i.e. virtual/bare-metal machines and
  containers);
* /glance/: the image store;
* /neutron/: the network manager for compute resources interconnection;
* /keystone/: manage authentication/authorization.

Each project are themselves based on multiple modules. Since OpenStack is
designed as a distributed software, each module can be deployed on different
physical/virtual machines. For instance, here is a set of modules that compose Nova:

* /nova-api/: in charge of managing users' requests;
* /nova-scheduler/: in charge of scheduling compute resources on compute nodes;
* /nova-compute/: in charge of the life-cycle of compute resources.
* ...

To provide all the features expected by an infrastructure manager, OpenStack's
modules need cooperation. For instance, when a user asks /nova/ to boot a VM, the
image is fetched from /glance/, its network interfaces are configured by
/neutron/, supposing /keystone/ authorized the operation. Such cooperation is
possible through three communication channels:

* /REST APIs/: used for inter-project communications;
* /Message queue/ (RabbitMQ): used for intra-project communications;
* /Database/ (MariaDB): used to store project states.

From the user viewpoint, OpenStack can be operated by three ways:

* Horizon: the OpenStack service in charge of providing a Web GUI;
* The OpenStack CLI;
* REST APIs.

Our OpenStack use-case
------------------------

Many projects exist to deploy OpenStack (e.g. OpenStack-Ansible,
OpenStack-Chef, OpenStack Kolla, Kubernetes, Juju). Our experiments
relies on a Kolla deployment of OpenStack. Kolla is an OpenStack
project which deploys OpenStack modules as Docker containers, using
Ansible.

It relies on 11 projects, gathering 36 modules. Following the roles
defined in Kolla's blueprints, we have defined 11 MAD components
whose names are mostly based on the OpenStack project they deploy:

1. *Nova* is in charge of provisioning compute instances (eg virtual
   machines)
2. *Glance* is the compute image store
3. *Neutron* is in charge of network resources
4. *Keystone* manages user authentication, authorization and service
   discovery
5. *MariaDB* is a SQL server used by most projects to store persistent
   information
6. *RabbitMQ* is the message bus for inter-service communication
7. *HAProxy* load-balances the requests to OpenStack APIs
8. *OpenVSwitch* virtualizes network functions
9. *Memcached* caches ephemeral data for most OpenStack services
10. *Facts* is a required component that collects information about
    every node
11. *Common* is meant to deploy utility services to each node (eg
    cron, fluentd: a metric collector for logging)

The following image represents the dependencies between these 11
components defined by using MAD.

.. image:: full.jpg
   :scale: 40 %
   :align: left

The detail of each component is not given in this documentation.
